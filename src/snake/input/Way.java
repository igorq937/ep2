package snake.input;

public enum Way {
    UP(1), DOWN(2), LEFT(3), RIGHT(4);

    public final int value;

    Way(int value) {
        this.value = value;
    }

}
