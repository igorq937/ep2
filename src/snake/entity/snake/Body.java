package snake.entity.snake;

import snake.Game;
import snake.entity.Entity;
import snake.graphics.Sprite;
import snake.rule.Rule;
import snake.tile.Terrain;

import java.awt.Graphics;

public class Body extends Entity {

    private int lastX, lastY;
    protected Game game;

    public Body(Game game, Rule rule, int x, int y) {
        super(rule, x, y);
        this.game = game;
        lastX = 0;
        lastY = 0;
    }

    @Override
    public void update() {
        Terrain.setSolidsId(0, lastX/Sprite.WIDTH, lastY/Sprite.HEIGHT);
        Terrain.setSolidsId(-1, x/Sprite.WIDTH, y/Sprite.HEIGHT);
    }

    @Override
    public void move(int x, int y) {
        lastX = this.x;
        lastY = this.y;
        this.x = x;
        this.y = y;

        update();
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Sprite.bodySnake, x, y, null);
    }

    public int getLastX() {
        return lastX;
    }

    public int getLastY() {
        return lastY;
    }
}
