package snake.entity.snake;

import snake.Game;
import snake.entity.Snake;
import snake.graphics.Sprite;
import snake.rule.Rule;
import snake.tile.Terrain;

import java.awt.Graphics;

public class Head extends Body {

    private Snake snake;

    public Head(Game game, Rule rule, Snake snake, int x, int y) {
        super(game, rule, x, y);
        this.snake = snake;
    }

    @Override
    public void update() {
        int i = x/Sprite.WIDTH;
        int j = y/Sprite.HEIGHT;
        if(Terrain.isSolid(i, j)) {
            rule.eat(game, Terrain.getSolidId(i, j), snake);
        }
    }

    @Override
    public void render(Graphics graphics) {
        graphics.drawImage(Sprite.headSnake, x, y, null);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
