package snake.entity.snake;

import snake.Game;
import snake.entity.Snake;
import snake.graphics.Sprite;
import snake.input.Way;
import snake.rule.Rule;
import snake.tile.Terrain;

public class Controller {

    private Game game;
    private Snake snake;
    private Rule rule;

    private boolean up;
    private boolean down;
    private boolean left;
    private boolean right;

    private Way lastWay;
    private int passo = 1;
    private final int PASSO_POR_TICK = 6;

    public Controller(Game game, Snake snake, Rule rule, Way initWay) {
        this.game = game;
        this.snake = snake;
        this.rule = rule;
        setMovement(initWay);
    }

    private void setMovement(Way way) {
        up = false;
        down = false;
        left = false;
        right = false;
        if(way == Way.UP) {
            up = true;
        }else if(way == Way.DOWN) {
            down = true;
        }else if(way == Way.LEFT) {
            left = true;
        }else if(way == Way.RIGHT) {
            right = true;
        }
    }

    public void update(int x, int y) {

        passo++;
    	
        int addY = Sprite.WIDTH;
        int addX = Sprite.HEIGHT;

        if(game.getKeyManager().left){
            setMovement(Way.LEFT);
        }
        else if(game.getKeyManager().right){
            setMovement(Way.RIGHT);
        }else if(game.getKeyManager().up) {
            setMovement(Way.UP);
        }
        else if(game.getKeyManager().down) {
            setMovement(Way.DOWN);
        }
        
        if(passo < PASSO_POR_TICK) return;
        
        if(left && lastWay != Way.RIGHT || lastWay == Way.LEFT){
            passo = 0;
            x -= addX;
            snake.move(x, y);
            lastWay = Way.LEFT;
        }
        else if(right && lastWay != Way.LEFT || lastWay == Way.RIGHT){
            passo = 0;
            x += addX;
            snake.move(x, y);
            lastWay = Way.RIGHT;
        }
        if(up && lastWay != Way.DOWN || lastWay == Way.UP) {
            passo = 0;
            y -= addY;
            snake.move(x, y);
            lastWay = Way.UP;
        }
        else if(down && lastWay != Way.UP || lastWay == Way.DOWN) {
            passo = 0;
            y += addY;
            snake.move(x, y);
            lastWay = Way.DOWN;
        }
        verifyLimits(x, y);
    }

    private void verifyLimits(int x, int y) {
        int i = x/Sprite.WIDTH;
        int j = y/Sprite.HEIGHT;
        if(!rule.haveBorder()) {
            if(i < 0) {
                x = Sprite.WIDTH*(Terrain.LIMIT_X-1);
                snake.move(x, y);
            }else if(i >= Terrain.LIMIT_X) {
                x = 0;
                snake.move(x, y);
            }else if(j < 0) {
                y = Sprite.HEIGHT*(Terrain.LIMIT_Y-1);
                snake.move(x, y);
            }else if(j >= Terrain.LIMIT_Y) {
                y = 0;
                snake.move(x, y);
            }
        }else{
            if(i < 0) {
                rule.gameOver(game);
            }else if(i >= Terrain.LIMIT_X) {
                rule.gameOver(game);
            }else if(j < 0) {
                rule.gameOver(game);
            }else if(j >= Terrain.LIMIT_Y) {
                rule.gameOver(game);
            }
        }
    }

}
