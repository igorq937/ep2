package snake.entity;

import snake.Game;
import snake.entity.snake.Body;
import snake.entity.snake.Controller;
import snake.entity.snake.Head;
import snake.graphics.Sprite;
import snake.input.Way;
import snake.rule.Rule;
import snake.tile.Terrain;

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;

public class Snake extends Entity{

    private Game game;
    private Rule rule;
    private Controller controller;

    private List<Body> bodies;
    private int grow = 0;

    private final int TAMANHO_INICIAL = 4;
    private final int TAMANHO_MAX = 230;
    private final Way initWay = Way.RIGHT;

    public Snake(Game game, Rule rule, int x, int y) {
        super(rule, x, y);
        this.game = game;
        this.rule = rule;
        bodies = new ArrayList<Body>();
        controller = new Controller(game, this, rule, initWay);

        createSnake(x, y);
    }

    private void createSnake(int x, int y) {
        bodies.add(new Head(game, rule, this, x, y));
        int j = x;
        for (int i = 1; i <= TAMANHO_INICIAL-1; i++){
            j--;
            bodies.add(new Body(game, rule, j, y));
        }
    }

    @Override
    public void update() {
        controller.update(x, y);
        updateGrow();
    }

    private void updateGrow() {

        if(bodies.size() >= TAMANHO_MAX) return;

        if(grow > 0) {
            int i = bodies.get(bodies.size()-1).getLastX()/ Sprite.WIDTH;
            int j = bodies.get(bodies.size()-1).getLastY()/Sprite.HEIGHT;
            bodies.add(new Body(game, rule, i, j));
            grow--;
        }else if(grow < 0) {
            int i = bodies.get(bodies.size()-1).x/Sprite.WIDTH;
            int j = bodies.get(bodies.size()-1).y/Sprite.HEIGHT;
            bodies.remove(bodies.size()-1);
            Terrain.setSolidsId(0, i, j);
            grow++;
        }

    }

    @Override
    public void move(int x, int y) {
        this.x = x;
        this.y = y;
        bodies.get(0).move(x, y);
        for (int i = 1; i < bodies.size(); i++) {
            bodies.get(i).move(bodies.get(i - 1).getLastX(), bodies.get(i - 1).getLastY());
        }
    }

    @Override
    public void render(Graphics graphics) {
        for (Body body : bodies) {
            body.render(graphics);
        }
    }

    public void increaseSize() {
        grow++;
    }

    public void resetSize() {
        for (int i = TAMANHO_INICIAL; i < bodies.size(); i++) {
            grow--;
        }
    }

}
