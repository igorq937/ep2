package snake.entity;

import snake.graphics.Sprite;
import snake.rule.Rule;

import java.awt.Graphics;

public abstract class Entity {

    protected int x, y;
    protected Rule rule;

    public Entity(Rule rule, int x, int y){
        this.rule = rule;
        this.x = x * Sprite.WIDTH;
        this.y = y * Sprite.HEIGHT;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public abstract void update();

    public abstract void move(int x, int y);

    public abstract void render(Graphics graphics);

}
