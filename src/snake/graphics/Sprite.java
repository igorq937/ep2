package snake.graphics;

import java.awt.image.BufferedImage;

public class Sprite {

    public static BufferedImage terrain, simpleFruit, bombFruit, bigFruit, decreaseFruit, headSnake, bodySnake, barrier;
    public static final int WIDTH = 30, HEIGHT = 30;
    public static final int WIDTH_TERRAIN = WIDTH*2, HEIGHT_TERRAIN = HEIGHT*2;

    public static void init() {
        SpriteManager spriteManager = new SpriteManager(ImageManager.load("/sprite.png"));
        terrain = spriteManager.getImagem(0 ,0, WIDTH_TERRAIN, HEIGHT_TERRAIN);
        simpleFruit = spriteManager.getImagem(WIDTH*2, 0, WIDTH, HEIGHT);
        bombFruit = spriteManager.getImagem(WIDTH*3, 0, WIDTH, HEIGHT);
        bigFruit = spriteManager.getImagem(WIDTH*2, HEIGHT, WIDTH, HEIGHT);
        decreaseFruit = spriteManager.getImagem(WIDTH*3, HEIGHT, WIDTH, HEIGHT);
        bodySnake = spriteManager.getImagem(0, HEIGHT*2, WIDTH, HEIGHT);
        headSnake = spriteManager.getImagem(0, HEIGHT*3, WIDTH, HEIGHT);
        barrier = spriteManager.getImagem(WIDTH, HEIGHT*2, WIDTH, HEIGHT);
    }

}
