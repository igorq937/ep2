package snake.graphics;

import java.awt.image.BufferedImage;

public class SpriteManager {

    private BufferedImage imagem;

    public SpriteManager(BufferedImage imagem) {
        this.imagem = imagem;
    }

    public BufferedImage getImagem(int x, int y, int width, int height) {
        return imagem.getSubimage(x, y, width, height);
    }
}
