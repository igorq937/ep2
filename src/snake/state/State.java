package snake.state;

import snake.Game;

import java.awt.Graphics;

public abstract class State {

    protected Game game;

    public State(Game game) {
        this.game = game;
    }

    private static State currentState = null;

    public static void setState(State state) {
        currentState = state;
    }

    public static State getState() {
        return currentState;
    }

    public abstract void update();

    public abstract void render(Graphics graphics);

}
