package snake.state;

import snake.Game;

import java.awt.Graphics;
import java.awt.Font;
import java.awt.Color;

public class MenuState extends State {

    private final String TITULO = game.getTitle();
    private Font tituloFont;

    private final String ESCOLHA = "Escolha sua Snake:";
    private Font escolhaFont;

    private final String DESCRICAO = "Pressione 'espaço' para confirmar";
    private Font descricaoFont;

    public static final String[] OPTIONS = {"KITTY", "COMMON", "STAR"};
    private Font optionsFont;
    private int choice;

    private final String[] OPTIONS_DESCRICAO = {"Com fronteiras sem barreiras", "Sem habilidade", "Dobro de pontos"};
    private Font optionsDescricaoFont;

    private final int DELAY = 5;
	private int contDelay;

    public MenuState(Game game) {
        super(game);
        tituloFont = new Font("Dialog", Font.BOLD,  80);
        escolhaFont = new Font("Dialog", Font.BOLD, 20);
        descricaoFont = new Font("Dialog", Font.BOLD, 20);
        optionsFont = new Font("Dialog", Font.BOLD,  30);
        optionsDescricaoFont = new Font("Dialog", Font.BOLD, 20);
        choice = 1;
    }

    @Override
    public void update() {

        if(contDelay <= DELAY) {
    		contDelay++;
    		return;
    	}else {
    		contDelay = 0;
    	}

        if(game.getKeyManager().space) {
            State.setState(new GameState(game, choice));
        }
        if(game.getKeyManager().up && choice > 0) {
            choice--;
        }
        if(game.getKeyManager().down && choice < OPTIONS.length-1) {
            choice++;
        }
    }

    @Override
    public void render(Graphics graphics) {
        renderBackground(graphics);
        renderTitulo(graphics);
        renderEscolha(graphics);
        renderOptions(graphics);
        renderOptionsDescricao(graphics);
        renderDescricao(graphics);
    }

    private void renderBackground(Graphics graphics) {
        graphics.setColor(Color.ORANGE);
        graphics.fillRect(0, 0, Game.getWidth(), Game.getHeight());
    }

    private void renderTitulo(Graphics graphics) {
        graphics.setColor(Color.BLACK);
        graphics.setFont(tituloFont);
        graphics.drawString(TITULO,
                Game.getWidth()/2 - graphics.getFontMetrics().stringWidth(TITULO)/2,
                Game.getHeight()*3/12);
    }

    private void renderEscolha(Graphics graphics) {
        graphics.setFont(escolhaFont);
        graphics.setColor(Color.GRAY);
        graphics.drawString(ESCOLHA,
                Game.getWidth()/2 - graphics.getFontMetrics().stringWidth(ESCOLHA)/2,
                Game.getHeight()*2/5);
    }

    private void renderOptions(Graphics graphics) {
        graphics.setFont(optionsFont);
        for(int i = 0; i < OPTIONS.length; i++) {
            if(choice == i) {
                graphics.setColor(Color.YELLOW);
            }else {
                graphics.setColor(Color.WHITE);
            }
            graphics.drawString(OPTIONS[i],
                    Game.getWidth()/2 - graphics.getFontMetrics().stringWidth(OPTIONS[i])/2,
                    Game.getHeight()*(6+i)/12);
        }
    }

    private void renderOptionsDescricao(Graphics graphics) {
        graphics.setFont(optionsDescricaoFont);
        graphics.setColor(Color.GRAY);
        graphics.drawString(OPTIONS_DESCRICAO[choice],
                    Game.getWidth()/2 - graphics.getFontMetrics().stringWidth(OPTIONS_DESCRICAO[choice])/2,
                    Game.getHeight()*9/12);
    }

    private void renderDescricao(Graphics graphics) {
        graphics.setFont(descricaoFont);
        graphics.setColor(Color.WHITE);
        graphics.drawString(DESCRICAO,
                Game.getWidth()/2 - graphics.getFontMetrics().stringWidth(DESCRICAO)/2,
                Game.getWidth() - 25);
    }

}
