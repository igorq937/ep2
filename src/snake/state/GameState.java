package snake.state;

import snake.Game;
import snake.entity.Snake;
import snake.rule.Rule;
import snake.state.game.FruitGenerator;
import snake.state.game.WorldGenerator;
import snake.tile.Terrain;

import java.awt.Graphics;

public class GameState extends State{

    private Snake snake;
    private FruitGenerator fruitGenerator;
    private WorldGenerator worldGenerator;

    private final int INIT_POSITION_X = 9;
    private final int INIT_POSITION_Y = 8;

    public GameState(Game game, int ruleId) {
        super(game);
        Terrain.init();
        snake = new Snake(game, Rule.rules[ruleId],INIT_POSITION_X, INIT_POSITION_Y);
        fruitGenerator = new FruitGenerator(snake);
        worldGenerator = new WorldGenerator(Rule.rules[ruleId]);
    }

    @Override
    public void update() {
        snake.update();
        fruitGenerator.update();
    }

    @Override
    public void render(Graphics graphics) {
        worldGenerator.render(graphics);
        snake.render(graphics);
    }

}
