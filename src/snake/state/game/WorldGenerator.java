package snake.state.game;

import snake.graphics.Sprite;
import snake.rule.Rule;
import snake.tile.Terrain;
import snake.tile.Tile;

import java.awt.Graphics;

public class WorldGenerator {

    public static final int[][] BARRIER_POSITIONS = {{1, 1}, {14, 1}, {1, 14}, {14, 14}};
    private Rule rule;

    public WorldGenerator(Rule rule) {
        this.rule = rule;
    }

    public void render(Graphics graphics) {
        renderWorld(graphics);
        renderBarriers(graphics);
    }

    private void renderWorld(Graphics graphics) {
        for (int i = 0; i < Terrain.LIMIT_X; i++){
            for (int j = 0; j < Terrain.LIMIT_Y; j++) {
                Tile.tiles[0].render(graphics, i, j);
            }
        }
    }

    private void renderBarriers(Graphics graphics) {
        int id;
        if(rule.haveBarrier()) {
            id = -1;
        }else{
            id = 0;
        }
        for (int i = 0; i < BARRIER_POSITIONS.length; i++){
            Terrain.setSolidsId(id, BARRIER_POSITIONS[i][0], BARRIER_POSITIONS[i][1]);
            graphics.drawImage(Sprite.barrier, BARRIER_POSITIONS[i][0]* Sprite.WIDTH, BARRIER_POSITIONS[i][1]* Sprite.HEIGHT, null);
        }
    }

}
