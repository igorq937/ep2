package snake.state.game;

import snake.entity.Snake;
import snake.graphics.Sprite;
import snake.tile.Terrain;
import snake.tile.fruit.Fruit;

import java.util.Random;

public class FruitGenerator {

    private Snake snake;

    private int[][] positionFruits;
    private int currentTick;
    private int howManyFruit;

    private final int FRUIT_TICK = 300;
    private final int DECREASE_TICK = 30;
    private final int MAX_FRUIT = 2;
    private final int SIMPLE_FRUIT_RATE = 20;
    private final int PROTECD_RANGE = 2;//Range a partir da cabeça da snake onde não surgirar frutas

    public FruitGenerator(Snake snake) {
        this.snake = snake;
        positionFruits = new int[MAX_FRUIT][2];//X e Y de cada Fruta
        this.currentTick = 0;
        this.howManyFruit = 0;
    }

    private void loadMap() {
        howManyFruit = 0;
        int y = 0;
        for (int i = 0; i < Terrain.LIMIT_X; i++){
            for (int j = 0; j < Terrain.LIMIT_Y; j++){
                if(Terrain.solidsId[i][j] > 0){
                    positionFruits[y][0] = i;
                    positionFruits[y][1] = j;
                    y++;
                    howManyFruit++;
                }
            }
        }
    }

    public void update() {

        loadMap();

        if(howManyFruit < MAX_FRUIT) {
            currentTick -= DECREASE_TICK;

            Random random = new Random();
            int i = random.nextInt(16);
            int j = random.nextInt(16);

            while((i >= snake.getX()/ Sprite.WIDTH-PROTECD_RANGE && i <= snake.getX()/ Sprite.WIDTH+PROTECD_RANGE) || Terrain.getSolidId(i , j) != null) {
                i = random.nextInt(16);
            }
            while((j >= snake.getY()/ Sprite.HEIGHT-PROTECD_RANGE && j <= snake.getY()/ Sprite.HEIGHT+PROTECD_RANGE) || Terrain.getSolidId(i , j) != null) {
                j = random.nextInt(16);
            }

            int id;
            int currentId = Fruit.SIMPLE_FRUIT.id;
            int cont = 0;

            while(true) {
                id = random.nextInt(3+ SIMPLE_FRUIT_RATE)+1;
                for (int a = 0; a < positionFruits.length; a++) {
                    if(Terrain.getSolidId(positionFruits[a][0], positionFruits[a][1]) != null) {
                        currentId = Terrain.getSolidId(positionFruits[a][0], positionFruits[a][1]).id;
                    }
                    if(currentId != id) {
                        cont++;
                    }
                }
                if(cont == MAX_FRUIT) {
                    if(id > 4) {
                        id = Fruit.SIMPLE_FRUIT.id;
                    }
                    Terrain.setSolidsId(id, i, j);
                    break;
                }
                cont = 0;
            }
        }

        if(currentTick == FRUIT_TICK) {
            for (int i = 0; i < positionFruits.length; i++) {
                Terrain.setSolidsId(0, positionFruits[i][0], positionFruits[i][1]);
            }
            howManyFruit = 0;
            currentTick = 0;
        }

        currentTick++;
    }
}
