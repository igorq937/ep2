package snake.state;

import snake.Game;
import snake.tile.Terrain;

import java.awt.Graphics;
import java.awt.Font;
import java.awt.Color;

public class GameOverState extends State {

    private String score;
    private String snake;
    private Font scoreFont;
    private Font snakeFont;

    private final String DESCRICAO = "Pressione 'espaço' para continuar";
    private Font descricaoFont;

    public GameOverState(Game game, int score, int choice) {
        super(game);
        Terrain.init();
        this.score = "SCORE: " + score;
        snake = "SNAKE: " + MenuState.OPTIONS[choice];
        scoreFont = new Font("Dialog", Font.BOLD,  50);
        snakeFont = new Font("Dialog", Font.BOLD,  20);
        descricaoFont = new Font("Dialog", Font.BOLD, 20);
    }

    @Override
    public void update() {
        if(game.getKeyManager().space) {
            State.setState(new MenuState(game));
        }
    }

    @Override
    public void render(Graphics graphics) {
        renderBackground(graphics);
        renderScore(graphics);
        renderSnake(graphics);
        renderDescricao(graphics);
    }

    private void renderBackground(Graphics graphics) {
        graphics.setColor(Color.PINK);
        graphics.fillRect(0, 0, Game.getWidth(), Game.getHeight());
    }

    private void renderScore(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.setFont(scoreFont);
        graphics.drawString(score,
                Game.getWidth()/2 - graphics.getFontMetrics().stringWidth(score)/2,
                Game.getHeight()*3/7);
    }

    private  void renderSnake(Graphics graphics) {
        graphics.setColor(Color.CYAN);
        graphics.setFont(snakeFont);graphics.drawString(snake,
                Game.getWidth()/2 - graphics.getFontMetrics().stringWidth(snake)/2,
                Game.getHeight()*4/8);

    }

    private void renderDescricao(Graphics graphics) {
        graphics.setFont(descricaoFont);
        graphics.setColor(Color.WHITE);
        graphics.drawString(DESCRICAO,
                Game.getWidth()/2 - graphics.getFontMetrics().stringWidth(DESCRICAO)/2,
                Game.getWidth() - 25);
    }

}
