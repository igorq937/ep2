package snake.tile;

import snake.graphics.Sprite;
import snake.tile.fruit.*;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class Tile {

    protected BufferedImage texture;
    protected final int id;

    public static Tile[] tiles = new Tile[5];
    public static Tile terrain = new Terrain(0);
    public static Tile simpleFruit = new SimpleFruit(Fruit.SIMPLE_FRUIT.id);
    public static Tile bigFruit = new BigFruit(Fruit.BIG_FRUIT.id);
    public static Tile bombFruit = new BombFruit(Fruit.BOMB_FRUIT.id);
    public static Tile decreaseFruit = new DecreaseFruit(Fruit.DECREASE_FRUIT.id);

    public Tile(BufferedImage texture, int id){
        this.texture = texture;
        this.id = id;

        tiles[id] = this;
    }

    public void render(Graphics graphics, int x, int y){
        graphics.drawImage(texture, x* Sprite.WIDTH, y* Sprite.HEIGHT, Sprite.WIDTH, Sprite.HEIGHT, null);
    }

    public int getId(){
        return id;
    }

}
