package snake.tile.fruit;

import snake.graphics.Sprite;
import snake.tile.Tile;

public class SimpleFruit extends Tile {
    public SimpleFruit(int id) {
        super(Sprite.simpleFruit, id);
    }
}
