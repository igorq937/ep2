package snake.tile.fruit;

import snake.graphics.Sprite;
import snake.tile.Tile;

public class BigFruit extends Tile {
    public BigFruit(int id) {
        super(Sprite.bigFruit, id);
    }
}
