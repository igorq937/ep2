package snake.tile.fruit;

import snake.graphics.Sprite;
import snake.tile.Tile;

public class DecreaseFruit extends Tile {
    public DecreaseFruit(int id) {
        super(Sprite.decreaseFruit, id);
    }
}
