package snake.tile.fruit;

import snake.graphics.Sprite;
import snake.tile.Tile;

public class BombFruit extends Tile {
    public BombFruit(int id) {
        super(Sprite.bombFruit, id);
    }
}
