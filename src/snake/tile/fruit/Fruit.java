package snake.tile.fruit;

public enum Fruit {
    SIMPLE_FRUIT(1), BIG_FRUIT(2), BOMB_FRUIT(3), DECREASE_FRUIT(4);

    public final int id;

    Fruit(int id) {
        this.id = id;
    }

}
