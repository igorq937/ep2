package snake.tile;

import snake.Game;
import snake.graphics.Sprite;
import snake.tile.fruit.Fruit;

import java.awt.Graphics;

import static snake.tile.fruit.Fruit.*;
import static snake.tile.fruit.Fruit.BOMB_FRUIT;

public class Terrain extends Tile {

    public static final int LIMIT_X = Game.getWidth()/ Sprite.WIDTH;
    public static final int LIMIT_Y = Game.getWidth()/ Sprite.HEIGHT;
    public static int[][] solidsId = new int[LIMIT_X][LIMIT_Y];

    public Terrain(int id) {
        super(Sprite.terrain, id);
    }

    public static void init() {
        for (int i = 0; i < LIMIT_X; i++){
            for (int j = 0; j < LIMIT_Y; j++){
                solidsId[i][j] = 0;
            }
        }
    }

    private void update(Graphics graphics) {
        int y = 0;
        for (int i = 0; i < LIMIT_X; i++){
            for (int j = 0; j < LIMIT_Y; j++){
                if(solidsId[i][j] > 0){
                    Tile.tiles[solidsId[i][j]].render(graphics, i, j);
                }
            }
        }
    }

    @Override
    public void render(Graphics graphics, int x, int y) {
        graphics.drawImage(texture, x* Sprite.WIDTH_TERRAIN, y* Sprite.HEIGHT_TERRAIN, Sprite.WIDTH_TERRAIN, Sprite.HEIGHT_TERRAIN, null);
        update(graphics);
    }

    public static boolean isSolid(int positionX, int positionY) {
        if(verifyPosition(positionX, positionY) == false) {
            return false;
        }
        if(solidsId[positionX][positionY] != 0) {
            return true;
        }else{
            return false;
        }
    }

    public static Fruit getSolidId(int positionX, int positionY) {
        if(verifyPosition(positionX, positionY) == false) {
            return null;
        }
        if(solidsId[positionX][positionY] == SIMPLE_FRUIT.id) {
            return SIMPLE_FRUIT;
        }else if(solidsId[positionX][positionY] == BIG_FRUIT.id) {
            return BIG_FRUIT;
        }else if(solidsId[positionX][positionY] == DECREASE_FRUIT.id) {
            return DECREASE_FRUIT;
        }else if(solidsId[positionX][positionY] == BOMB_FRUIT.id || solidsId[positionX][positionY] == -1){
            return BOMB_FRUIT;
        }else{
            return null;
        }
    }

    public static void setSolidsId(int id, int positionX, int positionY) {
        if(verifyPosition(positionX, positionY) == false) {
            return;
        }
        solidsId[positionX][positionY] = id;
    }

    private static boolean verifyPosition(int positionX, int positionY) {
        if(positionX < 0 || positionX >= LIMIT_X || positionY < 0 || positionY >= LIMIT_Y) {
            return false;
        }else{
            return true;
        }
    }

}
