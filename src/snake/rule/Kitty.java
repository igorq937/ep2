package snake.rule;

public class Kitty extends Rule {
    public Kitty(int id) {
        super(id);
    }

    @Override
    public boolean haveBarrier() {
        return false;
    }
}
