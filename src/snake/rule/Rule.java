package snake.rule;

import snake.Game;
import snake.entity.Snake;
import snake.state.GameOverState;
import snake.state.State;
import snake.tile.fruit.Fruit;

public class Rule {

    protected int score;
    public static final int INCREASE_SCORE = 50;

    protected final int id;

    public static Rule[] rules = new Rule[3];
    public static Rule kitty = new Kitty(0);
    public static Rule common = new Common(1);
    public static Rule star = new Star(2);

    public Rule(int id) {
        this.id = id;
        score = 0;
        rules[id] = this;
    }

    public void eat(Game game, Fruit fruit, Snake snake) {
        if(fruit == Fruit.SIMPLE_FRUIT){
            snake.increaseSize();
            score += INCREASE_SCORE;
        }else if(fruit == Fruit.BIG_FRUIT){
            snake.increaseSize();
            score += 2*INCREASE_SCORE;
        }else if(fruit == Fruit.BOMB_FRUIT){
            gameOver(game);
        }else if(fruit == Fruit.DECREASE_FRUIT){
            snake.resetSize();
        }
    }

    public boolean haveBarrier() {
        return true;
    }

    public boolean haveBorder() {
        return true;
    }

    public void gameOver(Game game) {
        State.setState(new GameOverState(game, score, id));
        score = 0;
    }

    public int getId(){
        return id;
    }

}
