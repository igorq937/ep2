package snake.rule;

import snake.Game;
import snake.entity.Snake;
import snake.tile.fruit.Fruit;

public class Star extends Rule {
    public Star(int id) {
        super(id);
    }

    @Override
    public void eat(Game game, Fruit fruit, Snake snake) {
        if(fruit == Fruit.SIMPLE_FRUIT){
            snake.increaseSize();
            score += 2*INCREASE_SCORE;
        }else if(fruit == Fruit.BIG_FRUIT){
            snake.increaseSize();
            score += 4*INCREASE_SCORE;
        }else if(fruit == Fruit.BOMB_FRUIT){
            gameOver(game);
        }else if(fruit == Fruit.DECREASE_FRUIT){
            snake.resetSize();
        }
    }

    @Override
    public boolean haveBorder() {
        return false;
    }

}
