package snake.rule;

public class Common extends Rule {
    public Common(int id) {
        super(id);
    }

    @Override
    public boolean haveBorder() {
        return false;
    }
}
