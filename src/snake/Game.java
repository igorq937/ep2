package snake;

import snake.display.Display;
import snake.graphics.Sprite;
import snake.input.KeyManager;
import snake.state.MenuState;
import snake.state.State;

import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferStrategy;

public class Game implements Runnable{

    private Display display;
    private static String title;
    private static int width;
    private static int height;
    private KeyManager keyManager;

    private static final double TICK_RATE = 14;

    public Game(String title, int width, int height) {
        this.title = title;
        this.width = width;
        this.height = height;

        keyManager = new KeyManager();
        display = new Display(title, width, height, keyManager);

        Sprite.init();
        State.setState(new MenuState(this));

        new Thread(this).start();
    }

    @Override
    public void run() {

        double tick = TICK_RATE;
        double wait = 0;
        long currentTime;
        long lastTime = System.nanoTime();

        //Game loop
        while(true) {
            currentTime = System.nanoTime();
            wait += tick - ((currentTime - lastTime)/1_000_000);

            update();
            render();

            if(wait > 0) {
                try {
                    Thread.sleep((long) tick);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lastTime = System.nanoTime();
                    wait = 0;
                }
            }

        }

    }

    public static String getTitle() {
        return title;
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    public KeyManager getKeyManager() {
        return keyManager;
    }

    private void update() {
        keyManager.update();

        if(State.getState() != null) {
            State.getState().update();
        }
    }

    private void render() {
        BufferStrategy preQuadros = display.getBufferStrategy();
        Graphics grafico = preQuadros.getDrawGraphics();

        grafico.clearRect(0, 0 , display.WIDTH, display.HEIGHT);

        if(State.getState() != null) {
            State.getState().render(grafico);
        }
        Toolkit.getDefaultToolkit().sync();

        preQuadros.show();
        grafico.dispose();
    }

}
