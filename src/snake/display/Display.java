package snake.display;

import snake.input.KeyManager;

import javax.swing.JFrame;
import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.image.BufferStrategy;

public class Display {

    private JFrame janela;
    private Canvas desenho;

    public final int WIDTH;
    public final int HEIGHT;
    private final int BUFFER = 3;

    public Display (String titulo, int width, int height, KeyManager keyManager) {
        janela = new JFrame(titulo);
        desenho = new Canvas();
        WIDTH = width;
        HEIGHT = height;

        createDisplay(keyManager);
    }

    private void createDisplay(KeyManager keyManager) {
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        janela.setResizable(false);
        janela.setLocationRelativeTo(null);
        janela.setVisible(true);

        Dimension dimensao = new Dimension(WIDTH, HEIGHT);
        desenho.setPreferredSize(dimensao);
        desenho.setMaximumSize(dimensao);
        desenho.setMinimumSize(dimensao);
        desenho.setFocusable(false);

        janela.add(desenho);
        janela.pack();
        janela.addKeyListener(keyManager);
        desenho.createBufferStrategy(BUFFER);
    }

    public BufferStrategy getBufferStrategy() {
        return desenho.getBufferStrategy();
    }

}
